import Phase from "./phase";
import Player from "./player";

export default class Ability {
  name: string;
  activate: (phase: Phase, otherPlayer: Player) => boolean;
  constructor(name: string, activate: (phase: Phase, otherPlayer: Player) => boolean) {
    this.name = name;
    this.activate = activate;
  }
}
