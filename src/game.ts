import Player from "./player";
import PlayerType from "./playerType";

export default class Game {
  atackingPlayer: Player;
  defendingPlayer: Player;
  constructor() {
    this.atackingPlayer = new Player("Character 1", PlayerType.Attacking);
    this.defendingPlayer = new Player("Character 2", PlayerType.Deffending);
  }
  run() {
    console.log(
      `${this.atackingPlayer.name} :  power = ${this.atackingPlayer.power}, defense = ${this.atackingPlayer.defense}`
    );
    console.log(
      `${this.defendingPlayer.name} :  power = ${this.defendingPlayer.power}, defense = ${this.defendingPlayer.defense}`
    );
    console.log("");
    if (Math.random() < 0.5) {
      this.switchPlayers();
    }
    let round = 0;

    while (this.atackingPlayer.health > 0) {
      round += 1;
      console.log(`Round ${round}:`);
      this.atackingPlayer.attack(this.defendingPlayer);

      console.log(
        `${this.defendingPlayer.name} has ${this.defendingPlayer.health} health`
      );
      console.log("");
      this.switchPlayers();
    }

    console.log(`${this.defendingPlayer.name} won`);
  }
  switchPlayers() {
    const tmpPlayer = this.atackingPlayer;
    this.atackingPlayer = this.defendingPlayer;
    this.defendingPlayer = tmpPlayer;
    this.atackingPlayer.type = PlayerType.Attacking;
    this.defendingPlayer.type = PlayerType.Deffending;
  }
}
