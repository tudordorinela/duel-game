import Ability from "./ability";
import Phase from "./phase";
import PlayerType from "./playerType";

export default class Player {
  name: string;
  health: number;
  defense: number;
  baseDefense: number;
  power: number;
  basePower: number;
  activeAbility: Ability | undefined;
  abilities: Ability[];
  hasActivatedAbility: boolean;
  type: PlayerType;
  constructor(name: string, type: PlayerType) {
    this.name = name;
    this.type = type;
    this.health = 100;
    this.baseDefense = Math.round(Math.random() * 5 + 10);
    this.defense = this.baseDefense;
    this.basePower = Math.round(Math.random() * 5 + 15);
    this.power = this.basePower;
    this.hasActivatedAbility = false;
    this.abilities = [
      new Ability("shield", (phase: Phase, otherPlayer: Player) => {
        if (phase == Phase.Main && this.type == PlayerType.Deffending) {
          const diff = Math.round((otherPlayer.power - this.defense) / 2);
          this.defense += diff;
          console.log(`${this.name} activates shield, reducing ${diff} damage`);
          this.hasActivatedAbility = true;
          return true;
        }
        return false;
      }),
      new Ability("powerUp", (phase: Phase, otherPlayer: Player) => {
        if (phase == Phase.Start && this.type == PlayerType.Attacking) {
          const diff = Math.round(this.power / 2);
          this.power += diff;
          console.log(`${this.name} activates powerUp, gaining ${diff} power`);
          this.hasActivatedAbility = true;
          return true;
        }
        return false;
      }),
      new Ability("heal", (phase: Phase, otherPlayer: Player) => {
        if (phase == Phase.End && this.type == PlayerType.Deffending) {
          if (this.health < 30) {
            this.health += 5;
            console.log(`${this.name} activates heal restoring 5 health`);
            this.hasActivatedAbility = true;
            return true;
          }
        }
        return false;
      }),
    ];
  }
  activateAbillity(phase: Phase, otherPlayer: Player) {
    if (this.activeAbility) this.activeAbility.activate(phase, otherPlayer);
  }
  attack(Pl2: Player) {
    this.setAbility();
    Pl2.setAbility();

    this.activateAbillity(Phase.Start, Pl2);
    Pl2.activateAbillity(Phase.Start, this);

    this.activateAbillity(Phase.Main, Pl2);
    Pl2.activateAbillity(Phase.Main, this);

    Pl2.health -= this.power - Pl2.defense;
    console.log(`${this.name} attacks`);

    this.activateAbillity(Phase.End, Pl2);
    Pl2.activateAbillity(Phase.End, this);

    if (!this.hasActivatedAbility && !Pl2.hasActivatedAbility)
      console.log("No ability activated");

    this.reset();
    Pl2.reset();
  }
  reset() {
    this.power = this.basePower;
    this.defense = this.baseDefense;
    this.hasActivatedAbility = false;
    this.activeAbility = undefined;
  }
  setAbility() {
    if (Math.random() < 0.25)
      this.activeAbility = this.abilities[
        Math.round(Math.random() * (this.abilities.length - 1))
      ];
  }
}
